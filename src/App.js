import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Jobs from "./pages/Jobs";
import JobDesc from "./pages/JobDesc";

function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<Jobs />} />
        <Route path="/Jobs/:uri" element={<JobDesc />} />
      </Routes>
    </Router>
  );
}

export default App;
