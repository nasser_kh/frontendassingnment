import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import axios from "axios";
import "./JobDesc.css";

function JobDesc() {
  const params = useParams();
  const [description, setDescription] = useState("");
  const [loading, setLoading] = useState(false);
  const [jobs, setJobs] = useState([]);

  useEffect(() => {
    const headers = {
      "accept-company": "900a776e-a060-422e-a5e3-979ef669f16b",
    };
    setLoading(true);
    axios
      .get(
        `https://devapi-indexer.elevatustesting.xyz/api/v1/jobs/uri?uri=${params.uri}&language_profile_uuid=ee5d991c-cdc6-4e83-b0b3-96f147208549`,
        { headers }
      )
      .then((response) => {
        setDescription(response.data.results);
        setLoading(false);
        console.log(response);
      });
  }, [params]);

  useEffect(() => {
    const headers = {
      "accept-company": "900a776e-a060-422e-a5e3-979ef669f16b",
    };
    axios
      .get(
        `https://devapi-indexer.elevatustesting.xyz/api/v1/jobs?language_profile_uuid=ee5d991c-cdc6-4e83-b0b3-96f147208549&limit=16&page=0`,
        { headers }
      )
      .then((response) => {
        setJobs(response.data.results.jobs);
      });
  }, []);

  const quary = jobs.map((job, index) => (
    <div className="jobs" key={`${index + 1}-job`}>
      <h1>Job Title:</h1>
      <h3>{job.title}</h3>
      <button>
        <Link to={`/Jobs/${job.uri}`}>Job description</Link>
      </button>
    </div>
  ));

  return (
    <div>
      <div className="sideBar">{quary}</div>

      {loading ? (
        <h4 className="Desc">Loading ...</h4>
      ) : (
        <div className="Desc">
          <h1>
            {description.title}
            <span className="jobType">{description.job_type}</span>
          </h1>
          <h5>Posted on: {description.posted_at}</h5>
          <h2>Description:</h2>
          <h5>
            <div
              dangerouslySetInnerHTML={{ __html: description.description }}
            />
          </h5>
          <h1>Requirments: </h1>
          <div className="li">
            <ul>
              <li>
                <div
                  dangerouslySetInnerHTML={{ __html: description.requirements }}
                />
              </li>
            </ul>
          </div>
          <h1>Required skills: </h1>
          <ul className="skills">
            <li className="liStkills">
              {description.skills ? description.skills[0] : ""}
            </li>
            <li className="liStkills">
              {description.skills ? description.skills[1] : ""}
            </li>
            <li className="liStkills">
              {description.skills ? description.skills[2] : ""}
            </li>
            <li className="liStkills">
              {description.skills ? description.skills[3] : ""}
            </li>
          </ul>
        </div>
      )}
    </div>
  );
}

export default JobDesc;
