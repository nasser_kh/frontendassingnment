import React, { useState, useEffect } from "react";
import ReactPaginate from "react-paginate";
import { Link } from "react-router-dom";
import axios from "axios";

function Jobs() {
  const [jobs, setJobs] = useState([]);
  const [currentPageNumber, setCurrentPageNumber] = useState(0);
  const [loading, setLoading] = useState(false);
  const [searchTitle, setSearchTitle] = useState("");
  const [pageCount, setPageCount] = useState(1);

  let jobsPerPage = 4;

  useEffect(() => {
    const headers = {
      "accept-company": "900a776e-a060-422e-a5e3-979ef669f16b",
    };
    const searchQuaryParam =
      searchTitle === "" ? "" : `&itemQuery=${searchTitle}`;
    setLoading(true);

    axios
      .get(
        `https://devapi-indexer.elevatustesting.xyz/api/v1/jobs?language_profile_uuid=ee5d991c-cdc6-4e83-b0b3-96f147208549&limit=${jobsPerPage}&page=${currentPageNumber}${searchQuaryParam}`,
        { headers }
      )
      .then((response) => {
        setJobs(response.data.results.jobs);
        setLoading(false);
        setPageCount(Math.floor(response.data.results.total / jobsPerPage));
        console.log(response.data.results.jobs);
        console.log(response);
      });
  }, [searchTitle, currentPageNumber, jobsPerPage]);

  const quary = jobs.map((job, index) => (
    <div className="job" key={`${index + 1}-job`}>
      <h1>Job Title :</h1>
      <h3>{job.title}</h3>
      <button className="descriptionBtn">
        <Link to={`/Jobs/${job.uri}`}>Job description</Link>
      </button>
    </div>
  ));
  const changePage = ({ selected }) => {
    setCurrentPageNumber(selected);
  };

  return (
    <div className="App">
      <ul className="ul1">
        <img src="./images/elevatus.png" alt="" />
      </ul>
      <ul className="ul">
        <input
          style={{
            width: "25%",
            height: "30px",
            margin: "20px",
          }}
          type="text"
          placeholder=" Search for Jobs ... "
          onKeyPress={(e) => {
            if (e.key === "Enter") {
              setSearchTitle(e.target.value);
              setCurrentPageNumber(0);
            }
          }}
        />
      </ul>
      <div className="cards">{loading ? <h4>Loading ...</h4> : quary}</div>
      <ReactPaginate
        previousLabel={"Previous"}
        nextLabel={"Next"}
        pageCount={pageCount}
        onPageChange={changePage}
        containerClassName={"paginationBttns"}
        previousLinkClassName={"previousBttn"}
        nextLinkClassName={"nextBttn"}
        disabledClassName={"paginationDisabled"}
        activeClassName={"paginationActive"}
      />
    </div>
  );
}
export default Jobs;
